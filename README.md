# Starting the Project

Use Visual Studio Code to start the project

## Repository Standards

Use the following commands to work with the repository

- Starting git flow
``
git glow init
``

- To add/finish new feature brach, APD refers to the initials of your name
``
git flow feature start APD_Feature_to_add
``
``
git flow feature finish APD_Feature_to_add
``

- To add/finish a release branch
``
git flow release start 0.0.1
``
``
git flow release finish '0.0.1'
``

- To add/finish a HotFix branch
``
git flow hotfix start APD_Hot_Fix 
``
``
git flow hotfix finish APD_Hot_Fix 
``

**At least two pull requests are required to merge the code into the depevolp branch** 
