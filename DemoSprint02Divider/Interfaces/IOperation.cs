﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoSprint02Divider.Interfaces
{
    public interface IOperation
    {
        public int Divide(int a, int b);
    }
}
