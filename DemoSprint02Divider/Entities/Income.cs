﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoSprint02Divider.Entities
{
    public class Income
    {
        public int firstOperando { get; set; }
        public int secondOperando { get; set; }
    }
}
