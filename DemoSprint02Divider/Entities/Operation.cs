﻿using DemoSprint02Divider.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoSprint02Divider.Entities
{
    public class Operation : IOperation
    {
        public int Divide(int a, int b)
        {
            if (b != 0)
            {
                return a / b;
            } else
            {
                return 111;
            }
        }
    }
}
