﻿using DemoSprint02Divider.Entities;
using DemoSprint02Divider.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoSprint02Divider.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DividerController : ControllerBase
    {
        private readonly IOperation operation;

        public DividerController(IOperation operation)
        {
            this.operation = operation;
        }

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet]
        [Route("[controller]")]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost]
        public ActionResult<int> Post([FromBody] Income income)
        {

            return Ok(new Response(operation.Divide(income.firstOperando, income.secondOperando)));
        }

    }
}
