﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoSprint02Divider.Entities
{
    public class Response
    {
        public int Total { get; set; }

        public Response(int value)
        {
            this.Total = value;
        }
    }
}
